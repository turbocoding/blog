---
layout: post
title:  "Recette - Omelette - Soufflée"
date:   2018-06-18 00:00:00 +0000
categories: recette omelette
description: Omelette souflée légère
---

<h2>Ingrédients</h2>
Pour deux personnes

* 2 jaunes d'oeufs
* 4 blancs d'oeufs
* 20 gr de farine
* 125 gr de lait
* 20 gr d'huile
* 100 gr de gorgonzola

Cuisson
* 1 min sur feu
* 5 min au four à 180°C

<h3>Pré-requis</h3>
Avoir une poelle qui peut aller au four

<h2>Instructions</h2>

* Mélanger les jaunes d'oeufs avec la farine et l'huile ;
* Faire chauffer le lait jusqu'à ébullition
* Mélanger les jaunes avec le lait et faire chauffer jusqu'à obtenir un léger épaississement
* Battre les blancs en neige jusqu'à obtenir un bec
* Incorporer la sauce de jaunes aux blancs
* Cuire 1 min à feu vif
* Mettre la poelle au four à 180°C pendant 5 min
    * A 3 min de cuisson ajouter le gorgonzola sur la moitié
* Replier l'omelette et badigeonner du reste de jaune d'oeuf  
