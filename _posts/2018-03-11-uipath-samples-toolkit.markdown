---
layout: post
title:  "UIPath - Samples - Toolkit"
date:   2018-03-11 00:00:00 +0000
categories: tutorial uipath
description: Tools for automation using UIPath of cases encountered frequently.
---

<h2>Introduction</h2>

The aim of this post is to publish a series of small [UIPath][UIPath] examples to help the development of automation scripts.
In the following examples, SAP is used. In order for UIPath to work with SAP, it's necessary to change user parameters of SAP

[Download samples][download-samples] or [Gitlab repository][gitlab-repository]

<h2>Regular expression</h2>

Find a specific pattern in a string. Example `ABC343232` will be found with `ABC\d{6}`.
To grab something in the beginning of the string use `^` and at the end of the line `$`.

File : regular-expression-begin-with-numbers.xaml

File : regular-expression-vat-number.xaml

<h2>List of objects</h2>

Initialise a list of string, push string into and retrieve values.

File : list-of-string.xaml

<h2>Wait for website to be loaded</h2>

In some cases, UIPath cannot select anything in a website if the source is dynamically modified. The source doesn't match what UIPath sees. In order to grab some specific information on the site, you need to wait for the web site to be fully loaded. This sample retrieve the information and check if a specific string is present on the site, if not it will wait for a few seconds and try again to find the specific string. After that the site can be considered as loaded.

File : wait-for-website-to-load.xaml

<h2>Grab table information into a DataTable</h2>

These examples takes excel sheet or CSV as input, import the content and store it into a DataTable. The content is the written in the console.
The content of such tables can be string, integer, etc, so it's mandatory to use GenericValue as variable type in UIPath.

File : csv-to-datatable.xaml

File : excel-get-data.xaml

<h2>Take a screenshot of the active screen</h2>

Take a screenshot of the active screen and store this image in a specific folder.

File : screenshot-save-into-folder.xaml

<h2>Non clickable link</h2>

Like in SAP, it's not always possible to click on a specific link because the frame cannot be separate in multiple links. The way to click on a specific link is to use "Text click". You will define the frame in which the link is present and define the visibile name of the string.

File : SAP-non-clickable.xaml

<h2>Change date format</h2>

When you need to generate a date time or get a date from a string that need to be formatted differently you can use a PARSEEXACT and fill the new format in a ToString("format")

File : change-date-format.xaml

<h2>Read a file and store it into an array of string (each line is a row)</h2>

If you need to read specific lines of a text file, you can use File.ReadAllLines and store these lines into an array of string.

File : readfile-into-string-table.xaml

<h2>Start a sequence with looping optionnal</h2>

When describing your process you'll need to loop on list of value and execute a series of actions. But when making modification on your script the loop can be really annoying because it take times to perform all these actions.
To prevent looping on sequence, you can put the sequence at the same level as the loop and bypass the loop, but some of the values on which you want to loop maybe not present any more. So the way to effectively prevent this problem is to check if the variable is empty and if so fill a static value to continue the process.

File : start-sequence-without-loop.xaml

<h2>Grab content of a website and store each line in a DataTable</h2>

Some web sites are not easily parsed by UIPath, in some case, it can be more effective to grab all the content and search for a pattern to grab a specific content. 

File : website-to-string-line-by-line.xaml

[Download samples][download-samples] or [Gitlab repository][gitlab-repository]

[download-samples]: https://gitlab.com/turbocoding/uipath-toolkit/repository/master/archive.zip
[gitlab-repository]: https://gitlab.com/turbocoding/uipath-toolkit
[UIPath]: http://www.uipath.com