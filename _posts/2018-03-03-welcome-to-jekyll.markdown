---
layout: post
title:  "Premier post du blog"
date:   2018-03-03 00:00:00 +0000
categories: firstpost update
---
Rappel pour la mise à jour du blog. Les fichiers contenant les posts se trouvent dans le répertoire `_posts`. 
Pour mettre à jour le site :
{% highlight ruby %}
git clone ...
bundle exec jekyll serve
// modification du site 
jekyll build
jekyll serve
git add
git commit
git push
{% endhighlight %}

Il est nécessaire de garder la convention `YYYY-MM-DD-name-of-post.ext`.

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
